package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func httpPort() string {
	port := os.Getenv("HTTP_PORT")
	if port != "" {
		return port
	}
	return "8080"
}

func appSecret() string {
	secret := os.Getenv("APPLICATION_SECRET")
	if secret != "" {
		return secret
	}
	return "0539d27ef84e60a300fe589a07df2bdbdb2c32f7d20784e2cbda2a855aea28e5"
}

func echoReplyBytes(msg []byte) []byte {
	return append(msg, []byte("\n")...)
}

func echo(w http.ResponseWriter, req *http.Request) {
	log.Printf("Request received from %v", req)

	if req.Body != nil {
		buffer, err := io.ReadAll(req.Body)
		if err != nil {
			http.Error(w, "Cannot read request body...", http.StatusInternalServerError)
			return
		}

		_, err = w.Write(echoReplyBytes(buffer))
		if err != nil {
			http.Error(w, "Cannot write response body...", http.StatusInternalServerError)
			return
		}
	}
}

func main() {
	http.HandleFunc("/echo", echo)

	address := fmt.Sprint(":", httpPort())
	log.Printf("Listening on %s", address)

	log.Printf("Application secret: %s", appSecret())

	err := http.ListenAndServe(address, nil)
	if err != nil {
		log.Panic(err)
	}
}
